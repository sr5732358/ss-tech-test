<?php

    /**
     * @param array $in
     * @return array $result
     */
    function reverse(array $in) {
        $result = [];
        for ($i=count($in) - 1; $i >= 0; $i--) {
            $result[] = $in[$i];
        }
        return $result;
    }

    $in = ['Apple', 'Banana', 'Orange', 'Coconut'];

    // Call reverse function
    $out = reverse($in);

    // Format and return response
    $result = json_encode($out);
    echo $result . "\n";



