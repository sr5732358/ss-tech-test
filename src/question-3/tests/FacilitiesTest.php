<?php

use Entities\Facility;
use Prophecy\Prophecy\ObjectProphecy;

class GuidelineEntityTest extends TestCase
{
    /**
     * @var Facility
     */
    private $entity;

    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $this->entity = new Facility();

        $this->entity->setId(1);
        $this->entity->setLocationCode('81082');
        $this->entity->setName('name');
        $this->entity->setStreetAddress('123 Street');
        $this->entity->setCity('Aguilar');
        $this->entity->setCounty('Las Animas');
        // and so on
    }

    public function testDefaultValues()
    {
        $entity = new Facility();

        $this->assertNull($entity->getId());
        $this->assertNull($entity->LocationCode());
        $this->assertNull($entity->getName());
        $this->assertInstanceOf(DateTimeImmutable::class, $entity->created_at);
        $this->assertInstanceOf(DateTimeImmutable::class, $entity->updated_at);
        //  and so on
    }

    public function testSetters()
    {
        $this->assertSame(1, $this->entity->getId());
        $this->assertSame('81082', $this->entity->getLocationCode());
        $this->assertSame('name', $this->entity->getName());
        $this->assertSame('123 Street', $this->entity->getStreetAddress());
        $this->assertSame('Aguilar', $this->entity->getStreetAddress());
        $this->assertSame('Lsd Animas', $this->entity->getCounty());
        // and so on
    }

    public function testToArray()
    {
        $result = $this->entity->toArray();
        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('location_code', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('street_address', $result);
        $this->assertArrayHasKey('city', $result);
        $this->assertArrayHasKey('county', $result);
        $this->assertArrayHasKey('formatted_county', $result);
        $this->assertArrayHasKey('state', $result);
        $this->assertArrayHasKey('postal_code', $result);
        $this->assertArrayHasKey('latitude', $result);
        $this->assertArrayHasKey('longitude', $result);
        $this->assertArrayHasKey('phone', $result);
        $this->assertArrayHasKey('contact_email', $result);
        $this->assertArrayHasKey('bre_group_id', $result);
    }

    public function testJsonSerialize()
    {
        $result = $this->entity->jsonSerialize();
        $this->assertIsArray($result);
    }
}